package com.chris.ggj13;

import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Bullet {
    public int strength;
    Body body;
    Vector2 tmp = new Vector2();
    Vector2 tmp2 = new Vector2();
    PointLight pointLight;
    private Sprite bulletSprite;

    public Bullet(int strength, GGJGame game, Player p) {
        this.strength = strength;
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        tmp.set(1, 0).setAngle(p.body.getAngle() * 180 / MathUtils.PI);
        tmp.add(game.rnd.nextFloat()/10f,game.rnd.nextFloat()/10f);
        tmp2.set(tmp);
        tmp.mul(2f).add(p.body.getWorldCenter());

        bodyDef.position.set(tmp);
        body = game.world.createBody(bodyDef);
        body.setBullet(true);
        CircleShape dynamicCircle = new CircleShape();
        dynamicCircle.setRadius(0.5f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = dynamicCircle;
        fixtureDef.density = 0.1f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 1f;
        fixtureDef.filter.groupIndex = 3;
        body.createFixture(fixtureDef);
        body.setUserData(this);
        tmp2.mul(2);
        body.applyLinearImpulse(tmp2, body.getWorldCenter());

        Vector2 pos = body.getPosition();
        pointLight = new PointLight(game.rayHandler, GGJGame.RAYS_NUM / 2,
                Color.WHITE, 5, pos.x, pos.y);
        pointLight.setSoft(false);
        pointLight.attachToBody(body, 0, 0);

        Texture texture = new Texture(Gdx.files.internal("data/bullet.png"));
        TextureRegion region = new TextureRegion(texture, 0, 0, 64, 64);
        bulletSprite = new Sprite(region);
        bulletSprite.setSize(1f, 1f);
        bulletSprite.setOrigin(bulletSprite.getWidth() / 2,
                bulletSprite.getHeight() / 2);
        bulletSprite.setPosition(-bulletSprite.getWidth() / 2, -bulletSprite.getHeight() / 2);

    }

    public void Draw(SpriteBatch sb) {
        Vector2 pos = body.getPosition();
        bulletSprite.setPosition(-bulletSprite.getWidth() / 2, -bulletSprite.getHeight() / 2);
        bulletSprite.translate(pos.x, pos.y);
        bulletSprite.draw(sb);
    }
}
