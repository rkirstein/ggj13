package com.chris.ggj13;

import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Heart {
    public Body body;
    public PointLight pointLight;

    static float[] beat = { 1, 1, 1, 1, 1.25f * 0.9f, 0.95f, 1.3f * 0.9f, 1, 1 };
    float heartBeatAnimSecs;
    private int life = 3;
    static private Sprite heartSprite;
    private static Texture texture;
    static final float HEARTANIMFRAMEDURATION = 0.096f;
    private GGJGame game;

    public Heart(GGJGame game, Vector2 p) {
        this.game = game;
        // Dynamic Body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(p);
        body = game.world.createBody(bodyDef);
        CircleShape dynamicCircle = new CircleShape();
        dynamicCircle.setRadius(3);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = dynamicCircle;
        fixtureDef.density = 0.5f;
        fixtureDef.filter.groupIndex = 4;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0.1f;
        body.createFixture(fixtureDef);
        body.setLinearDamping(5);
        body.setAngularDamping(1);
        body.setUserData(this);
        pointLight = new PointLight(game.rayHandler, GGJGame.RAYS_NUM);
        pointLight.attachToBody(body, 0, 0);
        pointLight.setColor(Color.RED);
        pointLight.setDistance(2);
    }

    public void update() {
        heartBeatAnimSecs += 0.016f;
    }

    public static void initGL() {
        texture = new Texture(Gdx.files.internal("data/Herz.png"));

        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        TextureRegion region = new TextureRegion(texture, 0, 0, 1024, 1024);
        heartSprite = new Sprite(region);
        heartSprite.setSize(6,6);
        heartSprite.setOrigin(heartSprite.getWidth() / 2, heartSprite.getHeight()/2);
        heartSprite.setPosition(-heartSprite.getWidth() / 2, -heartSprite.getHeight() / 2);

    }

    public void Draw(SpriteBatch sb) {
        heartSprite.setScale(beat[((int) (heartBeatAnimSecs / HEARTANIMFRAMEDURATION))
                            % beat.length]);
        heartSprite.setPosition(-heartSprite.getWidth() / 2, -heartSprite.getHeight() / 2);
        Vector2 pos = body.getPosition();
        heartSprite.translate(pos.x, pos.y);
        heartSprite.draw(sb);
    }

    public void hit() {
        life --;
        if (life <= 0)
            game.killeddeadHearts.add(this);
    }
}
