package com.chris.ggj13;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import box2dLight.ConeLight;
import box2dLight.Light;
import box2dLight.PointLight;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Player {
    public Body body;
    private float maxSpeed = 20.0f;
    private float maxTurn = 5.0f;
    private static final Integer RUN = 1;
    private static final Integer SHOOT = 2;

    public Light torchLight;
    public Light pointLight;
    public Tube connection;
    private static HashMap<Integer, Animation[]> Anims;
    private static HashMap<Integer, Animation[]> ColorMaps;
    private static Vector2[] SpriteWidth = { new Vector2(25.6f / 2, 22.4f / 2),
            new Vector2(25.6f / 2, 22.4f / 2),
            new Vector2(34.3f / 2, 22.4f / 2) };
    private int animID;

    private Color playerColor;

    private float animTimerSecs;
    private static Sprite animSprite;
    private static Sprite colorSprite;
    private static Vector2[] connectionPoints;
    private long lastShot;
    private int ammo;
    private float reloadSec=1;
    private float reloadTimer;
    
    private GGJGame game;
    static List<Color> Colors = new ArrayList<Color>();

    public Player(GGJGame game) {
        this.game = game;
        // Dynamic Body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(
                game.camera.viewportWidth / 2 + game.rnd.nextInt(40),
                game.camera.viewportHeight / 2 + game.rnd.nextInt(40));
        body = game.world.createBody(bodyDef);
        body.setLinearDamping(5f);
        body.setAngularDamping(1f);
        CircleShape dynamicCircle = new CircleShape();
        dynamicCircle.setRadius(3f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = dynamicCircle;
        fixtureDef.density = 3f;
        fixtureDef.filter.groupIndex = 99;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0.1f;
        body.setUserData(this);
        body.createFixture(fixtureDef);
        int colorid = game.rnd.nextInt(Colors.size());
        playerColor =Colors.get(colorid);
        Colors.remove(colorid);
                connection = new Tube(new Vector2(
                game.camera.viewportWidth / 2 - 1,
                game.camera.viewportHeight / 2 + 10 + 2), body.getPosition());
        connection.StartAnim(1, true);

    }

    public void update() {

        // torchLight.setPosition(body.getPosition());
        // torchLight.setDirection(body.getAngle()*180/MathUtils.PI);
        int angleindex = ((int) ((((body.getAngle() + Math.PI) * 180 / Math.PI)) + 540 - 22.5f) % 360) / 45;
        angleindex = angleindex < 0 ? 7 - (angleindex + 64) % 8
                : 7 - angleindex;

        Vector2 pos = body.getPosition();
        Vector2 cPt = connectionPoints[angleindex];
        connection.Update(new Vector3(pos.x + cPt.x, pos.y + cPt.y, 0), 0.016f);

        if (animID != 0) {
            if (Anims.get(animID)[0].isAnimationFinished(animTimerSecs)) {
                animID = 0;
                animTimerSecs = 0;
            } else {
                animTimerSecs += 0.016;
            }
        }
        
        if (ammo==0)
        {
            if(reloadTimer==0)connection.StartAnim(1, true);
            reloadTimer+= 0.016;
            
        }
        if (reloadTimer>=reloadSec / (float)game.upgradeLevel)
        {
            ammo+=2;
            reloadTimer=0;
        }
   
    }

    /**
     * Set moving direction
     * 
     * @param x
     * @param y
     */
    public void move(int x, int y) {
        x = x < -2 ? -1 : x > 2 ? 1 : 0;
        y = y < -2 ? -1 : y > 2 ? 1 : 0;
        Vector2 vel = body.getLinearVelocity();
        float vdx = vel.x;
        float accel = 1f;
        float currmaxSpeed = maxSpeed + 10 * game.upgradeLevel;
        switch (x) {
        case -1:
            vdx = Math.max(vel.x - accel, -currmaxSpeed);
            break;
        case 1:
            vdx = Math.min(vel.x + accel, +currmaxSpeed);
            break;
        default:
            break;
        }

        float vdy = vel.y;
        switch (y) {
        case -1:
            vdy = Math.max(vel.y - accel, -currmaxSpeed);
            break;
        case 1:
            vdy = Math.min(vel.y + accel, +currmaxSpeed);
            break;
        default:
            break;
        }
        float vx = vdx - vel.x;
        float vy = vdy - vel.y;
        float impulseX = body.getMass() * vx; // disregard time factor
        float impulseY = body.getMass() * vy; // disregard time factor
        float angle = body.getAngle();
        float timpulxeX = impulseX * MathUtils.cos(angle - MathUtils.PI / 2)
                - impulseY * MathUtils.sin(angle - MathUtils.PI / 2);
        float timpulxeY = impulseX * MathUtils.sin(angle - MathUtils.PI / 2)
                + impulseY * MathUtils.cos(angle - MathUtils.PI / 2);
        body.applyLinearImpulse(new Vector2(timpulxeX, timpulxeY),
                body.getWorldCenter());
        if (animID == 0 && (x != 0 || y != 0))
            animID = RUN;

    }

    public void turn(int dir) {
        float vel = body.getAngularVelocity();
        float accel = 1f;
        float vel2 = Math.max(vel - accel, -maxTurn * Math.signum(dir));

        float impulse = body.getMass() * (vel2 - vel); // disregard time factor

        body.applyAngularImpulse(impulse);

    }

    public static void initGl() {

        {

            Colors.add(new Color(86 / 255f, 131 / 255f, 183 / 255f, 1));
            Colors.add(new Color(183 / 255f, 86 / 255f, 165 / 255f, 1));
            Colors.add(new Color(183 / 255f, 123 / 255f, 86 / 255f, 1));
            Colors.add(new Color(84 / 255f, 183 / 255f, 86 / 255f, 1));
            Colors.add(new Color(123 / 255f, 123 / 255f, 123 / 255f, 1));
            Colors.add(new Color(239 / 255f, 52 / 255f, 57 / 255f, 1));
            Colors.add(new Color(127 / 255f, 86 / 255f, 183 / 255f, 1));
            Colors.add(new Color(255 / 255f, 100 / 255f, 0 / 255f, 1));
            Colors.add(new Color(255 / 255f, 198 / 255f, 133 / 255f, 1));
            Colors.add(new Color(205 / 205, 131 / 205, 183 / 255f, 1));
            // Animations
            Anims = new HashMap<Integer, Animation[]>();
            Animation[] anim1 = new Animation[8];
            Anims.put(RUN, anim1);
            Texture texture3;
            texture3 = new Texture(
                    Gdx.files.internal("data/final2048kontur.png"));
            texture3.setFilter(TextureFilter.Linear, TextureFilter.Linear);
            texture3.setWrap(TextureWrap.ClampToEdge, TextureWrap.Repeat);
            for (int i = 0; i < 8; ++i) {
                anim1[i] = new Animation(0.064f, new TextureRegion(texture3,
                        i * 256, 0, 256, 224), new TextureRegion(texture3,
                        i * 256, 224, 256, 224), new TextureRegion(texture3,
                        i * 256, 448, 256, 224), new TextureRegion(texture3,
                        i * 256, 224, 256, 224), new TextureRegion(texture3,
                        i * 256, 0, 256, 224), new TextureRegion(texture3,
                        i * 256, 672, 256, 224), new TextureRegion(texture3,
                        i * 256, 896, 256, 224), new TextureRegion(texture3,
                        i * 256, 672, 256, 224));
            }
            anim1 = new Animation[8];
            Anims.put(SHOOT, anim1);
            anim1[0] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 0 * 343, 1121, 343, 224));
            anim1[1] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 1 * 343 + 50, 1121, 343, 224));
            anim1[2] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 2 * 343 + 100, 1121, 343, 224));
            anim1[3] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 3 * 343 + 150, 1121, 343, 224));
            anim1[4] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 0 * 343, 1121 + 224, 343, 224));
            anim1[5] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 1 * 343 + 50, 1121 + 224, 343, 224));
            anim1[6] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 2 * 343 + 100, 1121 + 224, 343, 224));
            anim1[7] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 3 * 343 + 150, 1121 + 224, 343, 224));
        }
        {
            // ColorLAyer
            ColorMaps = new HashMap<Integer, Animation[]>();
            Animation[] anim1 = new Animation[8];
            ColorMaps.put(RUN, anim1);
            Texture texture3;
            texture3 = new Texture(
                    Gdx.files.internal("data/final2048color.png"));
            texture3.setFilter(TextureFilter.Linear, TextureFilter.Linear);
            texture3.setWrap(TextureWrap.ClampToEdge, TextureWrap.Repeat);
            for (int i = 0; i < 8; ++i) {
                anim1[i] = new Animation(0.064f, new TextureRegion(texture3,
                        i * 256, 0, 256, 224), new TextureRegion(texture3,
                        i * 256, 224, 256, 224), new TextureRegion(texture3,
                        i * 256, 448, 256, 224), new TextureRegion(texture3,
                        i * 256, 224, 256, 224), new TextureRegion(texture3,
                        i * 256, 0, 256, 224), new TextureRegion(texture3,
                        i * 256, 672, 256, 224), new TextureRegion(texture3,
                        i * 256, 896, 256, 224), new TextureRegion(texture3,
                        i * 256, 672, 256, 224));
            }
            anim1 = new Animation[8];
            ColorMaps.put(SHOOT, anim1);
            anim1[0] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 0 * 343, 1121, 343, 224));
            anim1[1] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 1 * 343 + 50, 1121, 343, 224));
            anim1[2] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 2 * 343 + 100, 1121, 343, 224));
            anim1[3] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 3 * 343 + 150, 1121, 343, 224));
            anim1[4] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 0 * 343, 1121 + 224, 343, 224));
            anim1[5] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 1 * 343 + 50, 1121 + 224, 343, 224));
            anim1[6] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 2 * 343 + 100, 1121 + 224, 343, 224));
            anim1[7] = new Animation(0.25f, new TextureRegion(texture3,
                    245 + 3 * 343 + 150, 1121 + 224, 343, 224));
        }

        animSprite = new Sprite();
        animSprite.setSize(25.6f / 2, 22.4f / 2);
        colorSprite = new Sprite();
        colorSprite.setSize(25.6f / 2, 22.4f / 2);

        connectionPoints = new Vector2[8];
        connectionPoints[7] = new Vector2(10.6f / 2 - 5, 1.9f / 2 + 1);
        connectionPoints[6] = new Vector2(11.2f / 2 - 5, 1.3f / 2 + 1);
        connectionPoints[5] = new Vector2(13.1f / 2 - 5, .7f / 2 + 2);
        connectionPoints[4] = new Vector2(14.4f / 2 - 5, 1.1f / 2 + 2);
        connectionPoints[3] = new Vector2(15.0f / 2 - 5, 1.5f / 2 + 2);
        connectionPoints[2] = new Vector2(14.4f / 2 - 6, 2.5f / 2 + 1);
        connectionPoints[1] = new Vector2(13.0f / 2 - 6, 3.0f / 2 + 1);
        connectionPoints[0] = new Vector2(11.0f / 2 - 5, 2.5f / 2 + 1);
        Tube.initGl();
    }

    public void Draw(SpriteBatch sb) {
        if (null == torchLight) {
            Vector2 pos = body.getPosition();
            torchLight = new ConeLight(game.rayHandler, GGJGame.RAYS_NUM,
                    playerColor, 80.0f, pos.x, pos.y, body.getAngle() * 180
                            / MathUtils.PI, 25f);
            torchLight.setSoft(true);
            torchLight.setSoftnessLenght(0.2f);
            pointLight = new PointLight(game.rayHandler, GGJGame.RAYS_NUM,
                    Color.WHITE, 20, pos.x, pos.y);

            pointLight.attachToBody(body, 0, 0);
            torchLight.attachToBody(body, 0, 0);
        }

        int numdirs = 8;
        float deg = (float) (((-body.getAngle()) * 180 / Math.PI));
        int angle = (int) ((deg % 360) + 360) % 360;
        int angleindex = (int) Math.floor(((angle + 180 / numdirs) % 360)
                / (360 / numdirs));
        Vector2 v = body.getPosition();
        if (animID != 0) {
            colorSprite.setSize(SpriteWidth[animID].x, SpriteWidth[animID].y);
            animSprite.setSize(SpriteWidth[animID].x, SpriteWidth[animID].y);
            colorSprite.setRegion(ColorMaps.get(animID)[angleindex]
                    .getKeyFrame(animTimerSecs));
            colorSprite.setPosition(-colorSprite.getWidth() / 2,
                    -colorSprite.getHeight() / 2);
            colorSprite.setColor(playerColor);
            colorSprite.translate(v.x + 1, v.y - 2);
            colorSprite.draw(sb);
            animSprite.setRegion(Anims.get(animID)[angleindex]
                    .getKeyFrame(animTimerSecs));
            animSprite.setPosition(-animSprite.getWidth() / 2,
                    -animSprite.getHeight() / 2);

            animSprite.translate(v.x + 1, v.y - 2);
            animSprite.draw(sb);

        } else {
            colorSprite.setSize(SpriteWidth[animID].x, SpriteWidth[animID].y);
            animSprite.setSize(SpriteWidth[animID].x, SpriteWidth[animID].y);
            colorSprite
                    .setRegion(ColorMaps.get(RUN)[angleindex].getKeyFrame(0));
            colorSprite.setPosition(-colorSprite.getWidth() / 2,
                    -colorSprite.getHeight() / 2);
            colorSprite.setColor(playerColor);
            colorSprite.translate(v.x + 1, v.y - 2);
            colorSprite.draw(sb);
            animSprite.setRegion(Anims.get(RUN)[angleindex].getKeyFrame(0));
            animSprite.setPosition(-animSprite.getWidth() / 2,
                    -animSprite.getHeight() / 2);

            animSprite.translate(v.x + 1, v.y - 2);
            animSprite.draw(sb);

        }

    }

    public void attack() {
        if (System.currentTimeMillis() - lastShot < 250)
            return;
        if(ammo==0)return;
        lastShot = System.currentTimeMillis();
        animTimerSecs = 0;
        animID = SHOOT;
        game.bullets.add(new Bullet(10, game, this));
        ammo-=1;       
    }

    public void removeSelf() {
        game.deadPlayers.add(this);
    }

}
