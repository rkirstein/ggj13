package com.chris.ggj13;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

public class ContactHandler implements ContactListener {
    private GGJGame game;

    public ContactHandler(GGJGame game) {
        super();
        this.game = game;

    }

    @Override
    public void beginContact(Contact contact) {
        // TODO Auto-generated method stub

    }

    @Override
    public void endContact(Contact contact) {
        // TODO Auto-generated method stub

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
        // TODO Auto-generated method stub

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
        Object userDataA = contact.getFixtureA().getBody().getUserData();
        Object userDataB = contact.getFixtureB().getBody().getUserData();
        Player p = null;
        Bullet b = null;
        Enemy e = null;
        Heart h = null;
        GGJGame base = null;
        if (userDataA instanceof GGJGame) {
            base = (GGJGame) userDataA;
        }
        if (userDataB instanceof GGJGame) {
            base = (GGJGame) userDataB;
        }

        if (userDataA instanceof Player) {
            p = (Player) userDataA;
        }
        if (userDataB instanceof Player) {
            p = (Player) userDataB;
        }
        if (userDataB instanceof Bullet) {
            b = (Bullet) userDataB;
        }
        if (userDataA instanceof Bullet) {
            b = (Bullet) userDataA;
        }
        if (userDataB instanceof Enemy) {
            e = (Enemy) userDataB;
        }
        if (userDataA instanceof Enemy) {
            e = (Enemy) userDataA;
        }
        if (userDataA instanceof Heart) {
            h = (Heart) userDataA;
        }
        if (userDataB instanceof Heart) {
            h = (Heart) userDataB;
        }

        // only do something if some colission is interesting
        if (null != e && null != p) {
            // TODO enemy hits player
        } else if (null != b && null != e) {
            // TODO bullet hits enemy
            e.hit(b);
            game.deadBullets.add(b);
        } else if (null != b && null != p) {
            // TODO bullet hits player
        } else if (null != h && null != p) {
            // TODO player collects heart
            p.connection.StartAnim(-1, true);
            game.deadHearts.add(h);
        } else if (null != b && null != h) {
            // TODO bullet hits heart
            h.hit();
            game.deadBullets.add(b);
        } else if (null != base && null != e) {
            // enemy hits base
            if (System.currentTimeMillis() - game.lastLiveLost > game.liveLostDt) {
                game.lastLiveLost = System.currentTimeMillis();
                game.lives--;
            }
        } else if (null != b) {
            // remove bullets
            game.deadBullets.add(b);
        }

    }

}
