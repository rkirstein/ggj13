package com.chris.ggj13;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class SnailMonster extends Enemy {

    static Sprite[] sprites;
    static Sprite heart;
    float[] beat = { 1, 1, 1, 1, 1.25f * 0.9f, 0.95f, 1.3f * 0.9f, 1, 1 };
    float heartBeatAnimSecs;
    static final float HEARTANIMFRAMEDURATION = 0.096f;

    public SnailMonster(GGJGame game, int x, int y) {
        super(game, x, y, 8);
     
    }

    @Override
    public void update() {
        super.update();
        heartBeatAnimSecs += 0.016f;

    }

    @Override
    public void Draw(SpriteBatch sb) {
        Vector2 pos = body.getPosition();
        heart.setScale(beat[((int) (heartBeatAnimSecs / HEARTANIMFRAMEDURATION))
                % beat.length]);
        heart.setPosition(-heart.getWidth() / 2, -heart.getHeight() / 2);
        heart.translate(pos.x, pos.y);
        heart.draw(sb);
        
        int numdirs = 4;
        float deg = angle + 90;
        int angle2 = (int) ((deg % 360) + 360) % 360;
        int angleindex = (int) Math.floor(((angle2 + 180 / numdirs) % 360)
                / (360 / numdirs));

        Sprite animSprite = sprites[angleindex];
        animSprite.setPosition(-animSprite.getWidth() / 2,
                -animSprite.getHeight() / 2);
        animSprite.translate(pos.x, pos.y);
        animSprite.draw(sb);
    }

    public static void initGL() {
        sprites = new Sprite[4];
        Texture tx = new Texture(Gdx.files.internal("data/Snaily.png"));
        tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        tx.setWrap(TextureWrap.ClampToEdge, TextureWrap.Repeat);
        for (int i = 0; i < 4; ++i) {

            TextureRegion region = new TextureRegion(tx, i * 256, 0, 256,
                    256);
            sprites[i] = new Sprite(region);
            sprites[i].setSize(25.6f/2*3, 25.6f/2*3);
            sprites[i].setOrigin(sprites[i].getWidth() / 2,
                    sprites[i].getHeight() / 2);
            sprites[i].setPosition(-sprites[i].getWidth() / 2,
                    -sprites[i].getHeight() / 2);
        }

        tx = new Texture(Gdx.files.internal("data/Herz.png"));

        tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        TextureRegion region = new TextureRegion(tx, 0, 0, 1024, 1024);

        heart = new Sprite(region);
        heart.setSize(6, 6);
        heart.setOrigin(heart.getWidth() / 2, heart.getHeight()/2);
        heart.setPosition(-heart.getWidth() / 2, -heart.getHeight() / 2);
    }

}
