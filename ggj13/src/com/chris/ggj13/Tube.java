package com.chris.ggj13;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class Tube {
    List<Vector3> points;
    List<Vector3> tangents;
    public static final int resolution = 8;
    public static final int CONNHEIGHT = 5;
    public static final float BUBBLESPEED = 0.7f;
    public static final float BUBBLEWIDTH = 1.0f;
    public static final float BUBBLELENGTH = 0.025f;
    public static final int HANG = -10;
    private float realdist = 0;
    CatmullRomSpline spline;
    static Texture texture;
    private static ShaderProgram shader;

    private Mesh mesh = null;
    private float bubblePos;
    private int bubbleDir;

    public void StartAnim(int dir, boolean reset) {
        bubbleDir = dir;
        if (reset)
        {
            if(dir==1)
            {
                bubblePos=0;
            }
            else if(dir ==-1)
            {
                bubblePos=1;
            }
        }
    }

    public Tube(Vector2 start, Vector2 end) {
        spline = new CatmullRomSpline();
        spline.add(new Vector3(start.x, start.y - CONNHEIGHT * 3f, 0));
        spline.add(new Vector3(start.x, start.y - CONNHEIGHT * 0, 0));
        spline.add(new Vector3());
        spline.add(new Vector3());
        spline.add(new Vector3());
        spline.add(new Vector3());
        spline.add(new Vector3());
        points = spline.getPath(resolution);
        // points.add(0, new Vector3(start.x,start.y,0));
        // points.add(new Vector3(end.x,end.y,0));

    }

    public static void initGl() {
        texture = new Texture(Gdx.files.internal("data/schlauchi.png"));
        texture.setWrap(TextureWrap.Repeat, TextureWrap.ClampToEdge);
        String vertexShader = "attribute vec4 a_position;    \n"
                + "attribute vec2 a_texCoord0;\n"
                + "uniform mat4 u_worldView;\n" + "varying vec2 v_texCoords;"
                + "void main()                  \n"
                + "{                            \n"
                + "   v_texCoords = a_texCoord0; \n"
                + "   gl_Position =  u_worldView * a_position;  \n"
                + "}                            \n";
        String fragmentShader = "#ifdef GL_ES\n" + "precision mediump float;\n"
                + "#endif\n" + "varying vec2 v_texCoords;\n"
                + "uniform sampler2D u_texture;\n"
                + "void main()                                  \n"
                + "{   " 
                + "  gl_FragColor = texture2D(u_texture, v_texCoords);\n" + "}";
        shader = new ShaderProgram(vertexShader, fragmentShader);
    }

    public void DrawDebug(ShapeRenderer shpr) {
        shpr.setColor(Color.WHITE);
        for (int i = 0; i < points.size() - 1; i++) {
            shpr.line(points.get(i).x, points.get(i).y, points.get(i + 1).x,
                    points.get(i + 1).y);
        }
    }

    public void Draw(Matrix4 matrix) {
        if (null == mesh)
            return;
        texture.bind();
        shader.begin();
        shader.setUniformMatrix("u_worldView", matrix);
        shader.setUniformi("u_texture", 0);
        Gdx.gl20.glEnable(GL20.GL_ALPHA);           
        mesh.render(shader,GL10.GL_TRIANGLE_STRIP);
        shader.end();
    }

    public void Update(Vector3 end, float elapsedsec) {
        if (null == mesh) {
            mesh = new Mesh(true, points.size() * 2, 0, VertexAttribute.Position(), VertexAttribute.TexCoords(0));
        }
        // anim durch den schlauch
        if (bubbleDir != 0) {
            switch (bubbleDir) {
            case -1:
                if (bubblePos <= -BUBBLELENGTH)
                    bubbleDir = 0;
                else
                    bubblePos += elapsedsec * BUBBLESPEED * bubbleDir;
                break;

            case 1:
                if (bubblePos >= 1+BUBBLELENGTH)
                    bubbleDir = 0;
                else
                    bubblePos += elapsedsec * BUBBLESPEED * bubbleDir;
                break;
            default:
                break;
            }
        }

        Vector3 start = spline.getControlPoints().get(0);
        Vector3 dir = end.tmp().sub(start).cpy();
        realdist = dir.len();
        spline.getControlPoints().get(2)
                .set(start.x + dir.x / 6, start.y + CONNHEIGHT * 4f, 0);
        spline.getControlPoints().get(3)
                .set(start.x + dir.x / 2, Math.min(start.y, end.y) - HANG, 0);
        spline.getControlPoints().get(4)
                .set(end.x - dir.x / 6, end.y + CONNHEIGHT * 1, 0);
        spline.getControlPoints().get(5).set(end.x, end.y + CONNHEIGHT * 0, 0);
        spline.getControlPoints().get(6).set(end.x, end.y - CONNHEIGHT * 2, 0);
        points = spline.getPath(resolution);
        tangents = spline.getTangents(resolution);
        float[] verts = new float[points.size() * 2 * 5];
        float len = 0;
        float dist = 0;
        Vector3 lastp = points.get(0);
        ;
        for (int i = 0; i < points.size(); ++i) {
            Vector3 p1 = points.get(i);
            dist += lastp.tmp().sub(p1).len();
            lastp = p1;
        }
        len = dist;
        dist = 0;
        lastp = points.get(0);
        float width = 25 / realdist;
        width = width < 0.6 ? 0.6f : width > 1f ? 1f : width;
        if (dir.y<0)
        for (int i = 0; i < points.size(); ++i) {

            Vector3 p1 = points.get(i);
            Vector3 t1 = tangents.get(i);
            dist += lastp.tmp().sub(p1).len();
            float ddl = dist / len;
            float rwidth = (width + (Math.abs(ddl - bubblePos) < BUBBLELENGTH ? BUBBLEWIDTH
                    : 0));
            verts[i * 10 + 0] = p1.x - t1.y * rwidth;
            verts[i * 10 + 1] = p1.y + t1.x * rwidth;
            verts[i * 10 + 2] = 0;
            verts[i * 10 + 3] = ddl * 4;
            verts[i * 10 + 4] = 0;
            verts[i * 10 + 5] = p1.x + t1.y * rwidth;
            verts[i * 10 + 6] = p1.y - t1.x * rwidth;
            verts[i * 10 + 7] = 0;
            verts[i * 10 + 8] = ddl * 4;
            verts[i * 10 + 9] = 1;
            lastp = p1;

        }
        else{
            int si= points.size()-1;
            lastp = points.get(si);
            for (int i = 0; i < points.size(); ++i) {

                Vector3 p1 = points.get(si-i);
                Vector3 t1 = tangents.get(si-i);
                dist += lastp.tmp().sub(p1).len();
                float ddl =(len- dist) / len;
                float rwidth = (width + (Math.abs(ddl - bubblePos) < BUBBLELENGTH ? BUBBLEWIDTH
                        : 0));
                verts[i * 10 + 0] = p1.x - t1.y * rwidth;
                verts[i * 10 + 1] = p1.y + t1.x * rwidth;
                verts[i * 10 + 2] = 0;
                verts[i * 10 + 3] = ddl * 4;
                verts[i * 10 + 4] = 0;
                verts[i * 10 + 5] = p1.x + t1.y * rwidth;
                verts[i * 10 + 6] = p1.y - t1.x * rwidth;
                verts[i * 10 + 7] = 0;
                verts[i * 10 + 8] = ddl * 4;
                verts[i * 10 + 9] = 1;
                lastp = p1;

            }}
        mesh.setVertices(verts);

        // FileHandle imageFileHandle = Gdx.files.internal("data/badlogic.jpg");
        // texture = new Texture(imageFileHandle);

    }
}
