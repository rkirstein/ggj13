package com.chris.ggj13;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

import box2dLight.ConeLight;
import box2dLight.PointLight;
import box2dLight.RayHandler;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.ContactFilter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class GGJGame implements ApplicationListener {
    public World world;
    Box2DDebugRenderer debugRenderer;
    public OrthographicCamera camera;
    public Matrix4 baseRenderMatrix;
    private SpriteBatch spritebatch;

    public RayHandler rayHandler;
    public static final int RAYS_NUM = 10;
    static final float BOX_STEP = 1 / 60f;
    static final int BOX_VELOCITY_ITERATIONS = 6;
    static final int BOX_POSITION_ITERATIONS = 2;
    static final float WORLD_TO_BOX = 0.01f;
    static final float BOX_TO_WORLD = 2;

    private Sprite splash1;
    private Sprite splash2;
    private Sprite splash3;
    private Sprite splash4;
    private Sprite splash5;
    static float startTimer = 0;

    static float monsterSpawnSec = 4;
    static float monsterTimerSec = 0;

    private float accDt;
    private Sprite Background;

    private Sprite playerBaseBack;
    private Sprite playerBase;
    private Sprite heartSprite;

    public int lives = 10;
    public int maxLives = 10;
    private int upgradeCurr = 0;
    private int upgradeNext = 5;
    public int upgradeLevel = 1;

    float[] beat = { 1, 1, 1, 1, 1.25f * 0.9f, 0.95f, 1.3f * 0.9f, 1, 1 };
    Vector2[] SpawnPos = { new Vector2(611.0f, 301.5f),
            new Vector2(611.0f, 301.5f), new Vector2(413.5f, 301.5f),
            new Vector2(517.0f, 243.0f), new Vector2(516.0f, 352.5f) };
    float heartBeatAnimSecs;
    static final float HEARTANIMFRAMEDURATION = 0.096f;

    // fixed seed
    Random rnd = new Random();
    public List<Player> players;
    List<Enemy> enemies;
    private FPSLogger fpsLog = new FPSLogger();
    HashSet<Enemy> deadEnemies = new HashSet<Enemy>();
    HashSet<Bullet> deadBullets = new HashSet<Bullet>();
    private PointLight baseLight;
    public ArrayList<Heart> hearts = new ArrayList<Heart>();
    public ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    public HashSet<Player> deadPlayers = new HashSet<Player>();
    public HashSet<Heart> deadHearts = new HashSet<Heart>();
    public HashSet<Heart> killeddeadHearts = new HashSet<Heart>();
    private ConeLight streetLight;
    private ConeLight streetLight2;
    private BitmapFont font;

    private Player lPlayer2 = null;
    private Player lPlayer3 = null;
    private Player lPlayer4 = null;
    public int liveLostDt = 1000;
    public long lastLiveLost = 0;

    private int state = 0;
    private long startMs;
    private long lastFrameTime;

    @Override
    public void create() {
        font = new BitmapFont(Gdx.files.internal("data/arial-15.fnt"),Gdx.files.internal("data/arial-15.png"),false);
        accDt = 0;
        camera = new OrthographicCamera();
        camera.viewportHeight = 600;
        camera.viewportWidth = 1024;
        camera.position.set(camera.viewportWidth * .5f,
                camera.viewportHeight * .5f, 0f);
        camera.zoom = 0.2f;
        camera.update();
        rayHandler = new RayHandler(world);
        rayHandler.setCombinedMatrix(camera.combined);
        // rayHandler.setShadows(true);
        // rayHandler.setCulling(true);
        RayHandler.setGammaCorrection(false);
        RayHandler.useDiffuseLight(true);
        // rayHandler.setAmbientLight(0.2f, 0.2f,0.2f, 0.2f);
        spritebatch = new SpriteBatch();
        world = new World(new Vector2(0, 0), true);
        world.setContactListener(new ContactHandler(this));
        world.setContactFilter(new ContactFilter() {
            @Override
            public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {

                boolean bothBullets = fixtureB.getFilterData().groupIndex == 3
                        && fixtureA.getFilterData().groupIndex == 3;
                if (bothBullets)
                    return false;

                boolean enemyOnHeart = (fixtureB.getFilterData().groupIndex == 4 && fixtureA
                        .getFilterData().groupIndex == 2)
                        || (fixtureB.getFilterData().groupIndex == 2 && fixtureA
                                .getFilterData().groupIndex == 4);
                if (enemyOnHeart)
                    return false;
                // monsters ignore walls
                return !((fixtureA.getFilterData().groupIndex == 1 && fixtureB
                        .getFilterData().groupIndex == 2) || (fixtureA
                        .getFilterData().groupIndex == 2 && fixtureB
                        .getFilterData().groupIndex == 1));
            }
        });

        debugRenderer = new Box2DDebugRenderer();

        Player.initGl();
        Augenmonster.initGL();
        SnailMonster.initGL();
        players = new ArrayList<Player>();
        players.add(new Player(this));

        enemies = new ArrayList<Enemy>();
        for (int i = 0; i < 5; i++) {
            Vector2 spawnpos = SpawnPos[rnd.nextInt(4)];
            if (rnd.nextFloat() > 0.5f)
                enemies.add(new Augenmonster(this, (int) spawnpos.x,
                        (int) spawnpos.y));
            else
                enemies.add(new SnailMonster(this, (int) spawnpos.x,
                        (int) spawnpos.y));
        }

        Heart.initGL();

        streetLight = new box2dLight.ConeLight(rayHandler, RAYS_NUM * 2,
                Color.YELLOW, 50, camera.viewportWidth / 2 + 25,
                camera.viewportHeight / 2 + 50, 250, 40);
        streetLight.setStaticLight(true);
        streetLight2 = new box2dLight.ConeLight(rayHandler, RAYS_NUM * 2,
                Color.YELLOW, 50, camera.viewportWidth / 2 + 30,
                camera.viewportHeight / 2 + 50, 250, 40);
        streetLight2.setStaticLight(true);

        new box2dLight.ConeLight(rayHandler, RAYS_NUM * 2,
                Color.RED, 50, camera.viewportWidth / 2,
                camera.viewportHeight / 2 - 10, 90, 40);
        streetLight2.setStaticLight(true);

        Texture texture;
        texture = new Texture(Gdx.files.internal("data/Basebottom.png"));

        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        TextureRegion region = new TextureRegion(texture, 0, 0, 1024, 1024);

        playerBaseBack = new Sprite(region);
        playerBaseBack.setSize(25.6f, 25.6f);
        playerBaseBack.setOrigin(playerBaseBack.getWidth() / 2,
                playerBaseBack.getHeight() / 2);
        playerBaseBack.setPosition(-playerBaseBack.getWidth() / 2,
                -playerBaseBack.getHeight() / 2);
        playerBaseBack.translate(camera.viewportWidth / 2,
                camera.viewportHeight / 2);
        texture = new Texture(Gdx.files.internal("data/Basetop.png"));
        Color baseColor = new Color(0, 180 / 256f, 1, 1f);
        baseLight = new PointLight(rayHandler, RAYS_NUM * 2, baseColor, 50,
                camera.viewportWidth / 2, camera.viewportHeight / 2);
        baseLight.setStaticLight(true);
        // baseLight.setSoft(true);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(camera.viewportWidth / 2,
                camera.viewportHeight / 2 - 5);
        Body baseBody = world.createBody(bodyDef);
        CircleShape dynamicCircle = new CircleShape();
        dynamicCircle.setRadius(10f);
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.filter.groupIndex = 99;
        fixtureDef.shape = dynamicCircle;
        fixtureDef.density = 3f;
        fixtureDef.friction = 0f;
        fixtureDef.restitution = 0f;
        baseBody.createFixture(fixtureDef);
        baseBody.setUserData(this);
        baseLight.attachToBody(baseBody, 0, 0);

        // world colliders
        // upper right
        fixtureDef.filter.groupIndex = 1;
        bodyDef.position.set(590.26666f, 344.7132f);
        Body b1 = world.createBody(bodyDef);
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(30, 20);
        fixtureDef.shape = shape;
        b1.createFixture(fixtureDef);

        // up
        bodyDef.position.set(450.26666f, 369.7132f);
        Body b2 = world.createBody(bodyDef);
        shape.setAsBox(1000, 10);
        fixtureDef.shape = shape;
        b2.createFixture(fixtureDef);

        // left
        bodyDef.position.set(399.76666f, 369.7132f);
        Body b3 = world.createBody(bodyDef);
        shape.setAsBox(10, 300);
        fixtureDef.shape = shape;
        b3.createFixture(fixtureDef);

        // right
        bodyDef.position.set(624.26666f, 369.7132f);
        Body b4 = world.createBody(bodyDef);
        shape.setAsBox(10, 300);
        fixtureDef.shape = shape;
        b4.createFixture(fixtureDef);

        // down
        bodyDef.position.set(450.26666f, 230.7132f);
        Body b5 = world.createBody(bodyDef);
        shape.setAsBox(1000, 10);
        fixtureDef.shape = shape;
        b5.createFixture(fixtureDef);

        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        region = new TextureRegion(texture, 0, 0, 1024, 1024);
        playerBase = new Sprite(region);
        playerBase.setSize(25.6f, 25.6f);
        playerBase.setOrigin(playerBase.getWidth() / 2,
                playerBase.getHeight() / 2);
        playerBase.setPosition(-playerBase.getWidth() / 2,
                -playerBase.getHeight() / 2);
        playerBase.translate(camera.viewportWidth / 2,
                camera.viewportHeight / 2);
        new ShapeRenderer();

        texture = new Texture(Gdx.files.internal("data/Herzsklein3.png"));

        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        region = new TextureRegion(texture, 0, 0, 240, 340);

        heartSprite = new Sprite(region);
        heartSprite.setSize(5.6f, 8);
        heartSprite.setOrigin(heartSprite.getWidth() / 2,
                heartSprite.getHeight());
        heartSprite.setPosition(-heartSprite.getWidth() / 2,
                -heartSprite.getHeight() / 2);
        heartSprite.translate(camera.viewportWidth / 2 - 1,
                camera.viewportHeight / 2 + 1.5f);

        texture = new Texture(Gdx.files.internal("data/battlefield.png"));

        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        region = new TextureRegion(texture, 0, 0, 1024, 600);

        Background = new Sprite(region);
        Background.setSize(102.4f * 2, 60.0f * 2);
        Background.translate(camera.viewportWidth / 2.5f,
                camera.viewportHeight / 2.5f);

        texture = new Texture(Gdx.files.internal("data/000Intro.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        region = new TextureRegion(texture, 0, 0, 1024, 600);
        splash1 = new Sprite(region);
        splash1.setSize(102.4f * 2, 60.0f * 2);
        splash1.translate(camera.viewportWidth / 2.5f,
                camera.viewportHeight / 2.5f);
        texture = new Texture(Gdx.files.internal("data/001Title.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        region = new TextureRegion(texture, 0, 0, 1024, 600);
        splash2 = new Sprite(region);
        splash2.setSize(102.4f * 2, 60.0f * 2);
        splash2.translate(camera.viewportWidth / 2.5f,
                camera.viewportHeight / 2.5f);
        texture = new Texture(Gdx.files.internal("data/002credits.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        region = new TextureRegion(texture, 0, 0, 1024, 600);
        splash3 = new Sprite(region);
        splash3.setSize(102.4f * 2, 60.0f * 2);
        splash3.translate(camera.viewportWidth / 2.5f,
                camera.viewportHeight / 2.5f);
        texture = new Texture(Gdx.files.internal("data/003steuerung.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        region = new TextureRegion(texture, 0, 0, 1024, 600);
        splash4 = new Sprite(region);
        splash4.setSize(102.4f * 2, 60.0f * 2);
        splash4.translate(camera.viewportWidth / 2.5f,
                camera.viewportHeight / 2.5f);
        texture = new Texture(Gdx.files.internal("data/fail.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        region = new TextureRegion(texture, 0, 0, 1024, 600);
        splash5 = new Sprite(region);
        splash5.setSize(102.4f * 2, 60.0f * 2);
        splash5.translate(camera.viewportWidth / 2.5f,
                camera.viewportHeight / 2.5f);

        new ShapeRenderer();
       
    }

    @Override
    public void dispose() {
        debugRenderer.dispose();
    }

    private void update() {
        if (state < 3) {
            if (state == 0
                    && (Gdx.input.isKeyPressed(Keys.P)
                            || Gdx.input.isKeyPressed(Keys.SPACE)
                            || Gdx.input.isKeyPressed(Keys.ENTER) || Gdx.input
                                .isButtonPressed(Buttons.LEFT))) {
                state = 1;
            }
            if (state == 1) {
                if (lPlayer2 == null
                        && (Gdx.input.isKeyPressed(Keys.ENTER) || Gdx.input
                                .isKeyPressed(Keys.UP))) {
                    lPlayer2 = new Player(this);
                    players.add(lPlayer2);
                }
                if (lPlayer3 == null
                        && (Gdx.input.isButtonPressed(Buttons.LEFT) || Gdx.input
                                .isButtonPressed(Buttons.RIGHT))) {
                    lPlayer3 = new Player(this);
                    players.add(lPlayer3);
                }
                if (lPlayer4 == null
                        && (Gdx.input.isKeyPressed(Keys.CONTROL_RIGHT) || Gdx.input
                                .isKeyPressed(Keys.I))) {
                    lPlayer4 = new Player(this);
                    players.add(lPlayer4);
                }
                if (Gdx.input.isKeyPressed(Keys.W)
                        || Gdx.input.isKeyPressed(Keys.I)
                        || Gdx.input.isButtonPressed(Buttons.RIGHT)
                        || Gdx.input.isKeyPressed(Keys.UP)){
                    state = 3;
                    startMs = System.currentTimeMillis();
                }
            }
            if (state == 0 && Gdx.input.isKeyPressed(Keys.C)) {
                state = 2;
            }
            if (state == 2
                    && (Gdx.input.isKeyPressed(Keys.B)
                            || Gdx.input.isButtonPressed(Buttons.LEFT)
                            || Gdx.input.isKeyPressed(Keys.ESCAPE) || Gdx.input
                                .isKeyPressed(Keys.ENTER)))
                state = 0;
            return;
        }

        {// Player1
            int dx = 0;
            int dy = 0;
            int dir = 0;
            if (Gdx.input.isKeyPressed(Keys.W))
                dy += 3;
            if (Gdx.input.isKeyPressed(Keys.S))
                dy -= 3;
            if (Gdx.input.isKeyPressed(Keys.A))
                dir -= 3;
            if (Gdx.input.isKeyPressed(Keys.D))
                dir += 3;
            if (Gdx.input.isKeyPressed(Keys.Q))
                dx -= 3;
            if (Gdx.input.isKeyPressed(Keys.E))
                dx += 3;
            players.get(0).move(dx, dy);
            players.get(0).turn(dir);

            if (Gdx.input.isKeyPressed(Keys.SPACE)) {
                players.get(0).attack();
            }

        }

        {// Player2
            if (lPlayer2 == null && Gdx.input.isKeyPressed(Keys.ENTER)) {
                lPlayer2 = new Player(this);
                players.add(lPlayer2);
            }
            if (lPlayer2 != null) {
                int dx = 0;
                int dy = 0;
                int dir = 0;
                if (Gdx.input.isKeyPressed(Keys.UP))
                    dy += 3;
                if (Gdx.input.isKeyPressed(Keys.DOWN))
                    dy -= 3;
                if (Gdx.input.isKeyPressed(Keys.LEFT))
                    dir -= 3;
                if (Gdx.input.isKeyPressed(Keys.RIGHT))
                    dir += 3;
                lPlayer2.move(dx, dy);
                lPlayer2.turn(dir);

                if (Gdx.input.isKeyPressed(Keys.ENTER)) {
                    lPlayer2.attack();
                }

            }

        }
        {// Player3
            if (lPlayer3 == null && Gdx.input.isButtonPressed(Buttons.LEFT)) {
                lPlayer3 = new Player(this);
                players.add(lPlayer3);
            }
            if (lPlayer3 != null) {
                int dir = 0;
                Vector2 pPos = new Vector2(lPlayer3.body.getPosition());
                Vector3 touchPos = new Vector3();
                touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
                camera.unproject(touchPos);
                Vector2 to = new Vector2(touchPos.x, touchPos.y);
                to.sub(pPos);
                float currAngle = MathUtils.radiansToDegrees
                        * lPlayer3.body.getAngle();
                currAngle = currAngle < 0 ? currAngle + 360 : currAngle;
                currAngle = currAngle % 360;
                float a = to.angle() - currAngle;
                a = (a + 180) % 360 - 180;

                if (a < 0 || a > 180)
                    dir = -3;
                else if (a > 0)
                    dir = 3;

                if (Gdx.input.isButtonPressed(Buttons.RIGHT))
                    lPlayer3.move(0, 3);
                if (Gdx.input.isButtonPressed(Buttons.MIDDLE))
                    lPlayer3.move(0, -3);
                lPlayer3.turn(-dir);

                if (Gdx.input.isButtonPressed(Buttons.LEFT)) {
                    lPlayer3.attack();
                }

            }

        }

        {// Player4
            if (lPlayer4 == null && Gdx.input.isKeyPressed(Keys.CONTROL_RIGHT)) {
                lPlayer4 = new Player(this);
                players.add(lPlayer4);
            }
            if (lPlayer4 != null) {
                int dx = 0;
                int dy = 0;
                int dir = 0;
                if (Gdx.input.isKeyPressed(Keys.I))
                    dy += 3;
                if (Gdx.input.isKeyPressed(Keys.K))
                    dy -= 3;
                if (Gdx.input.isKeyPressed(Keys.J))
                    dir -= 3;
                if (Gdx.input.isKeyPressed(Keys.L))
                    dir += 3;
                lPlayer4.move(dx, dy);
                lPlayer4.turn(dir);

                if (Gdx.input.isKeyPressed(Keys.CONTROL_RIGHT)) {
                    lPlayer4.attack();
                }

            }

        }
        
        
        if (lives <= 0)
            return;
        lastFrameTime = System.currentTimeMillis();
        
        float dt = Gdx.graphics.getDeltaTime();
        accDt += dt;
        while (accDt > dt) {
            for (Player p : players)
                p.update();

            for (Enemy e : enemies)
                e.update();

            for (Heart e : hearts)
                e.update();

            world.step(BOX_STEP, BOX_VELOCITY_ITERATIONS,
                    BOX_POSITION_ITERATIONS);
            accDt -= BOX_STEP;

            for (Enemy e : deadEnemies) {
                e.pointLight.remove();
                hearts.add(new Heart(this, e.body.getPosition()));
                world.destroyBody(e.body);
            }
            enemies.removeAll(deadEnemies);
            deadEnemies.clear();
            for (Bullet b : deadBullets) {
                b.pointLight.remove();
                world.destroyBody(b.body);
            }
            bullets.removeAll(deadBullets);
            deadBullets.clear();

            for (Player b : deadPlayers) {
                b.pointLight.remove();
                b.torchLight.remove();
                world.destroyBody(b.body);
            }
            players.removeAll(deadPlayers);
            deadPlayers.clear();
            for (Heart b : killeddeadHearts) {
                b.pointLight.remove();
                world.destroyBody(b.body);
            }
            for (Heart b : deadHearts) {
                b.pointLight.remove();
                world.destroyBody(b.body);
                if (lives < maxLives)
                    lives++;
                upgradeCurr++;
                if (upgradeCurr >= upgradeNext) {
                    levelUp();
                    upgradeLevel++;
                }

            }
            hearts.removeAll(deadHearts);
            hearts.removeAll(killeddeadHearts);
            killeddeadHearts.clear();
            deadHearts.clear();
            heartBeatAnimSecs += 0.016f;
        }
        monsterTimerSec += dt;
        if (monsterTimerSec > monsterSpawnSec
                && enemies.size() < upgradeLevel * 3) {
            monsterTimerSec -= monsterSpawnSec;
            for (int i = 0; i < players.size(); i++) {
                System.out.println("spawned!");
                Vector2 spawnpos = SpawnPos[rnd.nextInt(4)];
                if (rnd.nextFloat() > 0.5f)
                    enemies.add(new Augenmonster(this, (int) spawnpos.x,
                            (int) spawnpos.y));
                else
                    enemies.add(new SnailMonster(this, (int) spawnpos.x,
                            (int) spawnpos.y));
            }
        }
    }

    private void levelUp() {
        upgradeCurr = 0;
    }

    @Override
    synchronized public void render() {
        update();
        if (state == 0) {
            spritebatch.setProjectionMatrix(camera.combined);
            spritebatch.begin();
            splash2.draw(spritebatch);
            spritebatch.end();
            return;
        } else if (state == 1) {
            spritebatch.begin();
            splash4.draw(spritebatch);
            spritebatch.end();
            return;
        } else if (state == 2) {
            spritebatch.begin();
            splash3.draw(spritebatch);
            spritebatch.end();
            return;
        }

        // fpsLog.log();

        spritebatch.setProjectionMatrix(camera.combined);

        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        spritebatch.begin();
        Background.draw(spritebatch);

        for (Player p : players) {
            p.Draw(spritebatch);
        }
        for (Enemy p : enemies) {
            p.Draw(spritebatch);
        }
        for (Bullet p : bullets) {
            p.Draw(spritebatch);
        }
        for (Heart p : hearts) {
            p.Draw(spritebatch);
        }

        spritebatch.end();
        for (Player p : players) {
            if (p.body.getPosition().y > camera.viewportHeight / 2)
                p.connection.Draw(camera.combined);
        }
        spritebatch.begin();
        playerBaseBack.draw(spritebatch);

        heartSprite
                .setScale(beat[((int) (heartBeatAnimSecs / HEARTANIMFRAMEDURATION))
                        % beat.length]);
        heartSprite.draw(spritebatch);
        playerBase.draw(spritebatch);
        spritebatch.end();

        //debugRenderer.render(world, camera.combined);
        for (Player p : players) {
            if (p.body.getPosition().y < camera.viewportHeight / 2)
                p.connection.Draw(camera.combined);
        }
        // light everything
        rayHandler.updateAndRender();

        // if (Gdx.input.isTouched()) {
        // Vector3 touchPos = new Vector3();
        // touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        // camera.unproject(touchPos);
        // System.out.println(touchPos.x + "," + touchPos.y);
        // }

        
        if (lives<=0)
        {
            spritebatch.begin();
               splash5.draw(spritebatch);
               spritebatch.end();
        }
       

        float tmpZoom = camera.zoom;
        camera.zoom = 1;
        camera.update();
        spritebatch.setProjectionMatrix(camera.combined);
        spritebatch.begin();
        font.draw(spritebatch, "Upgrade: " + upgradeCurr + "/" + upgradeNext,
                25, 582);
        font.draw(spritebatch, "Live: " + lives + "/" + maxLives, 25, 552);
        font.draw(spritebatch, "Level: " + upgradeLevel, 25, 522);
        
        font.draw(spritebatch, "Your Score: " + (lastFrameTime - startMs) / 1000, 25, 492);
        spritebatch.end();
        camera.zoom = tmpZoom;
        camera.update();
        spritebatch.setProjectionMatrix(camera.combined);

    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
        accDt = 0;
    }
}
