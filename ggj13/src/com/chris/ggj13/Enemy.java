package com.chris.ggj13;

import box2dLight.Light;
import box2dLight.PointLight;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Enemy {

    public Body body;
    private float maxSpeed = 2.0f;
    protected GGJGame game;
    Light pointLight;
    private int health = 30;
    private Vector2 tmp = new Vector2();
    protected float angle;

    public Enemy(GGJGame game, int x, int y, float r) {
        this.game = game;
        // Dynamic Body
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(x + game.rnd.nextFloat(), y + game.rnd.nextFloat());
        body = game.world.createBody(bodyDef);
        CircleShape dynamicCircle = new CircleShape();       
        dynamicCircle.setRadius(r);        
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = dynamicCircle;
        fixtureDef.density = 5f;
        fixtureDef.friction = 1f;
        fixtureDef.restitution = 0.1f;
        fixtureDef.filter.groupIndex = 2;
        body.createFixture(fixtureDef);
        body.setUserData(this);
        pointLight = new PointLight(game.rayHandler, GGJGame.RAYS_NUM);
        pointLight.attachToBody(body, 0, 0);
        pointLight.setColor(Color.RED);
        pointLight.setDistance(0);
    }

    public void Draw(SpriteBatch sb) {

    }

    public void update() {
        if (body.getUserData() != this)
            System.err.println("mismatched user data");

        tmp.set(512,300-5);
        tmp.sub(body.getWorldCenter());
        tmp.nor();
        angle = tmp.angle();

        Vector2 vel = body.getLinearVelocity();
        tmp.add(vel);
        tmp.x = tmp.x > maxSpeed ? maxSpeed : tmp.x < -maxSpeed ? -maxSpeed
                : tmp.x;
        tmp.y = tmp.y > maxSpeed ? maxSpeed : tmp.y < -maxSpeed ? -maxSpeed
                : tmp.y;

        tmp.sub(vel).mul(body.getMass());
        body.applyLinearImpulse(tmp, body.getWorldCenter());
    }

    public void hit(Bullet b) {
        health -= b.strength;
        if (health <= 0)
            die();
    }

    private void die() {
        game.deadEnemies.add(this);
    }
}
