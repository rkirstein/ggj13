package com.chris.ggj13Client;

import java.io.IOException;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chris.ggj13.net.Network;
import com.chris.ggj13.net.Network.Move;
import com.esotericsoftware.kryonet.Client;

public class GGJ13Client implements ApplicationListener {
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private Texture texture;
    private Sprite sprite;
    private Client client;

    @Override
    public void create() {
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        camera = new OrthographicCamera(1, h / w);
        batch = new SpriteBatch();

        texture = new Texture(Gdx.files.internal("data/libgdx.png"));
        texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        TextureRegion region = new TextureRegion(texture, 0, 0, 512, 275);

        sprite = new Sprite(region);
        sprite.setSize(0.9f, 0.9f * sprite.getHeight() / sprite.getWidth());
        sprite.setOrigin(sprite.getWidth() / 2, sprite.getHeight() / 2);
        sprite.setPosition(-sprite.getWidth() / 2, -sprite.getHeight() / 2);

        client = new Client();
        client.start();
        Network.register(client);
        try {
            client.connect(500, "172.16.23.13", Network.port, Network.portUdp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dispose() {
        batch.dispose();
        texture.dispose();
    }

    @Override
    public void render() {
        if (!client.isConnected())
            Gdx.gl.glClearColor(1, 0, 0, 1);
        else
            Gdx.gl.glClearColor(0, 1, 0, 1);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        // sprite.draw(batch);
        batch.end();

        int accelX = (int) -Gdx.input.getPitch();
        int accelY = (int) Gdx.input.getRoll();
        int accelZ = (int) Gdx.input.getAccelerometerZ();
        Move move = new Move();
        move.x = accelX;
        move.y = accelY;
        client.sendUDP(move);
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {
        client.close();
    }

    @Override
    public void resume() {
        client.start();
        Network.register(client);
        try {
            client.connect(500, "172.16.23.13", Network.port, Network.portUdp);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
